FOLDER="akbar at `Date`"
mkdir "$FOLDER"
cd "$FOLDER"
mkdir about_me
cd about_me
mkdir personal
mkdir professional
cd personal
echo 'https://www.facebook.com/tegarimansyahfb' > 'facebook.txt'
cd ..
cd professional
echo 'https://www.linkedin.com/in/muhammad-akbar-al-hakim/' > 'linkedin.txt'
cd ..
cd ..
mkdir my_friends
cd my_friends
api_friends=$(curl --location --request GET 'https://gist.githubusercontent.com/tegarimansyah/e91f335753ab2c7fb12815779677e914/raw/94864388379fecee450fde26e3e73bfb2bcda194/list%20of%20my%20friends.txt')
echo "$api_friends" > 'list_of_my_friends.txt'
cd ..
mkdir my_system_info
cd my_system_info
username=$(id -un)
host=$(uname -a)
echo "My username : $username \nWith host: $host" > "about_this_laptop.txt"
checkping=$(ping -c 3 google.com)
echo "Connection to google: \n$checkping "> 'internet_connection.txt'
